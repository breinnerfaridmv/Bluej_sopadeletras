package Negocio;



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class SopaLetraTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class SopaLetraTest
{
    /**
     * Default constructor for test class SopaLetraTest
     */
    public SopaLetraTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void testCrearMatriz() throws Exception
    {
     String prueba="ana,ma,p";
     SopaLetra s=new SopaLetra(prueba);
     char rta[][]=s.getSopa();
     char ideal_matriz[][]={{'a','n','a'},{'m','a'},{'p'}};
     //si rta == ideal_matriz --> :)
     boolean paso=esIgual(rta, ideal_matriz);
     assertEquals(true,paso);
    
    }
    
    
    @Test
    public void testCrearMatriz2() throws Exception
    {
     String prueba="ana,maaa";
     SopaLetra s=new SopaLetra(prueba);
     char rta[][]=s.getSopa();
     char ideal_matriz[][]={{'a','n','a'},{'m','a','a','a'}};
     //si rta == ideal_matriz --> :)
     boolean paso=esIgual(rta, ideal_matriz);
     assertEquals(true,paso);
    
    }
    
    @Test
    public void testCrearMatriz3() throws Exception
    {
     String prueba="ana,maria,pepe,juan";
     SopaLetra s=new SopaLetra(prueba);
     char rta[][]=s.getSopa();
     char ideal_matriz[][]={{'a','n','a'},{'m','a','r','i','a'},{'p','e','p','e'},{'j','u','a','n'}};
     //si rta == ideal_matriz --> :)
     boolean paso=esIgual(rta, ideal_matriz);
     assertEquals(true,paso);
    
    }
    
    @Test
    public void testCrearMatrizMal() throws Exception
    {
     String prueba="ana,ma,p";
     SopaLetra s=new SopaLetra();
     char rta[][]=s.getSopa();
     char ideal_matriz[][]={{'a','n','a'},{'m','a'},{'p'}};
     //si rta == ideal_matriz --> :)
     boolean paso=esIgual(rta, ideal_matriz);
     assertEquals(false,paso);
    
    }
    
     @Test
    public void testCrearMatrizMal2() throws Exception
    {
     String prueba="anamaria,marco,pepe";
     SopaLetra s=new SopaLetra(prueba);
     char rta[][]=s.getSopa();
     char ideal_matriz[][]={{'a','n','a'},{'m','a','r','c','o'},{'p'}};
     //si rta == ideal_matriz --> :)
     boolean paso=esIgual(rta, ideal_matriz);
     assertEquals(false,paso);
    
    }
    
    @Test
    public void testEsCuadrada() throws Exception {
     String prueba="pepe,pepe,pepe,pepe";
     SopaLetra s=new SopaLetra(prueba);
     boolean esCuadrada = s.esCuadrada();
     assertEquals(true,esCuadrada); 
    }
    
    @Test
    public void testEsCuadrada2() throws Exception {
     String prueba="anama,marco,pepes,juane,andre";
     SopaLetra s=new SopaLetra(prueba);
     boolean esCuadrada = s.esCuadrada();
     assertEquals(true,esCuadrada);
  
    }
    
    @Test
    public void testEsCuadrada3() throws Exception {
     String prueba="juanes,marcos,julian,marlay,camilo,andres";
     SopaLetra s=new SopaLetra(prueba);
     boolean esCuadrada = s.esCuadrada();
     assertEquals(true,esCuadrada);
  
    }
     
    @Test
    public void testNoEsCuadrada() throws Exception {
     String prueba="juanes,marco,luis,alejandra,pepe,jorge,alberto";
     SopaLetra s=new SopaLetra(prueba);
     boolean esCuadrada = s.esCuadrada();
     assertEquals(false,esCuadrada);
  
    }
    
    @Test
    public void testNoEsCuadrada2() throws Exception {
     String prueba="andres,juan,farid,maria,sergio,mauricio";
     SopaLetra s=new SopaLetra(prueba);
     boolean esCuadrada = s.esCuadrada();
     assertEquals(false,esCuadrada);
  
    }
    
    @Test
    public void testEsRectangular() throws Exception {
     String prueba= "breiner,alejora,juliana,marlene,camilos,andrese";
     SopaLetra s=new SopaLetra(prueba);
     boolean esRectangular = s.esRectangular();
     assertEquals(true,esRectangular);
   
    }
    @Test
    public void testEsRectangular2() throws Exception {
     String prueba= "jorge,pedro,juane,marco";
     SopaLetra s=new SopaLetra(prueba);
     boolean esRectangular = s.esRectangular();
     assertEquals(true,esRectangular);
   
    }
    @Test
    public void testEsRectangular3() throws Exception {
     String prueba= "andres,julian,matias,albert,marlay";
     SopaLetra s=new SopaLetra(prueba);
     boolean esRectangular = s.esRectangular();
     assertEquals(true,esRectangular);
   
    }
    @Test
    public void testNoEsRectangular() throws Exception {
     String prueba= "anama,marco,pepes,juane,andre";
     SopaLetra s=new SopaLetra(prueba);
     boolean esRectangular = s.esRectangular();
     assertEquals(false,esRectangular);
   
    }
    @Test
    public void testNoEsRectangular2() throws Exception {
     String prueba= "gabriel,juan,farid,marco,luis,jorge";
     SopaLetra s=new SopaLetra(prueba);
     boolean esRectangular = s.esRectangular();
     assertEquals(false,esRectangular);
   
    }
    @Test
    public void testEsDispersa() throws Exception {
     String prueba="juanes,marco,luis,alejandra,pepe,jorge,alberto";
     SopaLetra s=new SopaLetra(prueba);
     boolean esDispersa = s.esDispersa();
     assertEquals(true,esDispersa); 
    }
    
    @Test
    public void testEsDispersa2() throws Exception {
     String prueba= "andres,juan,farid,maria,sergio,mauricio";
     SopaLetra s=new SopaLetra(prueba);
     boolean esDispersa = s.esDispersa();
     assertEquals(true,esDispersa); 
    }
    
    @Test
    public void testEsDispersa3() throws Exception {
     String prueba="gabriel,juan,farid,marco,luis,jorge";
     SopaLetra s=new SopaLetra(prueba);
     boolean esDispersa = s.esDispersa();
     assertEquals(true,esDispersa); 
    }
    
    @Test
    public void testNoDispersa() throws Exception {
     String prueba="pepe,pepe,pepe,pepe";
     SopaLetra s=new SopaLetra(prueba);
     boolean esDispersa = s.esDispersa();
     assertEquals(false,esDispersa); 
    }
    
    @Test
    public void testNoDispersa2() throws Exception {
     String prueba="juanes,marcos,julian,marlay,camilo,andres";
     SopaLetra s=new SopaLetra(prueba);
     boolean esDispersa = s.esDispersa();
     assertEquals(false,esDispersa); 
    }
    
    @Test
    public void testCharMasSeRepite() throws Exception {
     String prueba="juanes,marco,luis,alejandra,pepe,jorge,alberto";
     SopaLetra s=new SopaLetra(prueba);
     char caracter  = s.get_MasSeRepite();
     char esperado = 'a';
     boolean esIgual = caracter == esperado;
     assertEquals(true,esIgual);
    }
    
    @Test
    public void testCharMasSeRepite2() throws Exception {
     String prueba="ivy,juan,farid,maria,sergio,mauricio";
     SopaLetra s=new SopaLetra(prueba);
     char caracter  = s.get_MasSeRepite();
     char esperado = 'i';
     boolean esIgual = caracter == esperado;
     assertEquals(true,esIgual);
    }
    
    @Test
    public void testCharMasSeRepite3() throws Exception {
     String prueba="anam,marco,pepes,juane,andre,jorge,jeny";
     SopaLetra s=new SopaLetra(prueba);
     char caracter  = s.get_MasSeRepite();
     char esperado = 'e';
     boolean esIgual = caracter == esperado;
     assertEquals(true,esIgual);
    }
   
    @Test
    public void testCharMasSeRepite4() throws Exception {
     String prueba="juanes,marcos,julian,marlay,camilo,andres";
     SopaLetra s=new SopaLetra(prueba);
     char caracter  = s.get_MasSeRepite();
     char esperado = 'm';
     boolean esIgual = caracter == esperado;
     assertEquals(false,esIgual);
    }
    
    @Test
    public void testCharMasSeRepite5() throws Exception {
     String prueba="anamaria,marco,pepe";
     SopaLetra s=new SopaLetra(prueba);
     char caracter  = s.get_MasSeRepite();
     char esperado = 'p';
     boolean esIgual = caracter == esperado;
     assertEquals(false,esIgual);
    }
    
    @Test
    public void testDiagonalInferior() throws Exception {
     String prueba="pepe,pepe,pepe,pepe";
     SopaLetra s=new SopaLetra(prueba);
     char diagonal [] = s.getDiagonarInferior();
     char esperada[] = {'e','p','e','p','e','e'};
     boolean esIgual = sonIgualesVectores(diagonal,esperada);
     assertEquals(true,esIgual);
    }
    
    @Test
    public void testDiagonalInferior2() throws Exception {
     String prueba="anama,marco,pepes,juane,andre";
     SopaLetra s=new SopaLetra(prueba);
     char diagonal [] = s.getDiagonarInferior();
     char esperada[] = {'n','a','m','a','r','c','o','e','s','e'};
     boolean esIgual = sonIgualesVectores(diagonal,esperada);
     assertEquals(true,esIgual);
    }
    
    @Test
    public void testDiagonalInferior3() throws Exception {
     String prueba="juanes,marcos,julian,marlay,camilo,andres";
     SopaLetra s=new SopaLetra(prueba);
     char diagonal [] = s.getDiagonarInferior();
     char esperada[] = {'u','a','n','e','s','r','c','o','s','i','a','n','a','y','o'};
     boolean esIgual = sonIgualesVectores(diagonal,esperada);
     assertEquals(true,esIgual);
    }
    
    @Test
    public void testDiagonalInferior4() throws Exception {
     String prueba="ana,uva,ave";
     SopaLetra s=new SopaLetra(prueba);
     char diagonal [] = s.getDiagonarInferior();
     char esperada[] = {'a','n','a'};
     boolean esIgual = sonIgualesVectores(diagonal,esperada);
     assertEquals(false,esIgual);
    }
   
    @Test
    public void testDiagonalInferior5() throws Exception {
     String prueba="juan,luis,pepe,leny";
     SopaLetra s=new SopaLetra(prueba);
     char diagonal [] = s.getDiagonarInferior();
     char esperada[] = {'u','a','n','s','i','e'};
     boolean esIgual = sonIgualesVectores(diagonal,esperada);
     assertEquals(false,esIgual);
    }
    
    private boolean esIgual(char m1[][], char m2[][])
     {
 
      if(m1==null || m2==null || m1.length != m2.length)
        return false;
        
      for(int i1=0,i2=0;i1<m1.length && i2<m2.length;i1++,i2++)
      {
        if(!sonIgualesVectores(m1[i1],m2[i2]))
                return false;
        
      }
      return true;
    
    }
    
    private boolean sonIgualesVectores(char v1[],char v2[])
    {
        if(v1.length != v2.length)
            return false;
            
        for(int j=0;j<v1.length;j++)
           {
                if(v1[j]!=v2[j])
                    return false;
           }
       return true;
    }
    
}