package Negocio;

/**
 * Write a description of class SopaLetra here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SopaLetra implements OperacionMatriz
{
    // La matriz con las letras de la sopa
    private char sopa[][];

    /**
     * Constructor for objects of class SopaLetras
     */
    public SopaLetra()
    {

    }

    public SopaLetra(String palabras) throws Exception
    {
        if(palabras==null || palabras.isEmpty())
        {
            throw new Exception("No se puede crear la sopa");
        }

        String palabras2[]=palabras.split(",");
        //Crear la cantidad de filas:
        this.sopa=new char[palabras2.length][];
        //recorrer cada elemento de palabras2 y pasarlo a su correspondiente fila en sopa:
        int i=0;
        for(String palabraX: palabras2)
        {
            this.sopa[i]=new char[palabraX.length()];
            pasar(palabraX, this.sopa[i]);
            i++;
        }

    }

    // Pasar cada char del String al arreglo vector de chars
    private void pasar(String x, char vector[])
    {
        for(int j=0;j<x.length();j++)
            vector[j]=x.charAt(j);
    }

    public String toString(){
        String msg="";
        for(int i=0;i<this.sopa.length;i++){

            for (int j=0;j<this.sopa[i].length;j++)
            {
                msg+=this.sopa[i][j]+""+"\t";
            }
            msg+="\n";
        }
        return msg;
    }

    public String toString2(){
        String msg="";
        for(char vector[]:this.sopa){
            for (char dato:vector)
            {
                msg+=dato+""+"\t";
            }
            msg+="\n";
        }
        return msg;
    }

    /**
    Retorna la letra que más se repite
     */
    public char get_MasSeRepite()
    {
        int masRepetida=0;
        int contadortmp = 0;
        char caracter = 'o';
        char sopatmp [][] = this.getSopa();

        for(int i =  0 ; i<sopatmp.length;i++){
            for(int j = 0 ; j<sopatmp[i].length ; j++){
                char caractertmp = sopatmp[i][j];

                for(int a =  0 ; a<sopatmp.length;a++){
                    for(int b = 0 ; b<sopatmp[a].length ; b++){
                        if(caractertmp == sopatmp[a][b]){
                            contadortmp++;
                        }   
                    }  
                }
                if(contadortmp>masRepetida){
                    masRepetida = contadortmp;
                    caracter = caractertmp;
                }
                contadortmp = 0;

            }
        }

        return caracter;   

    }

    public boolean esCuadrada(){
        boolean esCuadrada = true;
        if(this.sopa != null){
            int filas = this.sopa.length;
            int columnas = 0;
            for(int i = 0 ; i<this.sopa.length && esCuadrada;i++){
                columnas = this.sopa[i].length;
                if(filas == columnas){
                    esCuadrada = true;
                }
                else{
                    esCuadrada = false;
                }
            }       
        }
        return esCuadrada;
    }

    public boolean esRectangular(){
        boolean esRectangular =  true;
        if(this.sopa != null){
            int filas =  this.sopa.length;
            int columnas1 = 0;
            int columnas2 = 0;
            for(int i = 0 ; i<this.sopa.length-1 && esRectangular;i++){
                columnas1= this.sopa[i].length;
                columnas2 = this.sopa[i+1].length;
                if(filas != columnas1 && columnas1 == columnas2){
                    esRectangular =  true;
                }
                else{
                    esRectangular = false;
                }
            }
        }
        return esRectangular;
    }

    public boolean esDispersa(){
        boolean esDispersa = false;
        if(!esCuadrada() && !esRectangular()) esDispersa = true;
        return esDispersa;
    }

    public char []getDiagonarInferior() throws Exception
    {
        char diagonal [];
        //si y solo si es cuadrada , si no, lanza excepcion
        if(!esCuadrada()) throw new Exception("La matriz no es cuadrada");
        int tamaño = 0;
        int filas  =  this.sopa.length;
        for(int i=0; i<filas;i++){
            tamaño += (filas-i) - 1;
        }
        diagonal= new char [tamaño];
        int x = 0;
        for(int i=0;i<this.sopa.length;i++){
            for (int j=i;j<this.sopa[i].length-1;j++)
            {
                diagonal[x]= sopa[i][j+1];
                x++;
            }
        }
        return diagonal;
    }

    public int getCantCol(int fila)
    {
        if(sopa==null||fila>=sopa.length || fila<0 )
            return -1;

        return (sopa[fila].length);
    }

    public int filaMasDatos()
    {
        int posicion=0;
        
        char sopatmp[][]=this.getSopa();
        int v[]=new int[sopatmp.length];
        for(int i=0; i<sopatmp.length;i++){
         
            v[i]=sopa[i].length;
            
        }
        
        int a=0;
        int tmp=0;
        for(int k=0; k<v.length; k++){
            
            if(v[k]>a){
            a=v[k];
            tmp=k;
            
            }
          
        }
        return tmp+1;
    }

    //Start GetterSetterExtension Code
    /**Getter method sopa*/
    public char[][] getSopa(){
        return this.sopa;
    }//end method getSopa

    /**Setter method sopa*/
    public void setSopa(char[][] sopa){
        this.sopa = sopa;
    }//end method setSopa

    
    //End GetterSetterExtension Code
    //!
}
